# Script to calculate the growth rate threshold 'r_limite', from an input combination of parameters, under which there is evolutionary rescue
# that is, under which the population goes extinct if it does not invade the resistant compartment
# For that, simulation of the demographic evolution of the population without virulent allele, varying the growth rate r

import numpy as np
# import sympy
# from sympy import Poly
# from sympy.abc import x
# from sympy import solveset, S, solve_univariate_inequality
# from sympy.solvers.inequalities import solve_poly_inequality

import os.path
import os
#import csv
import time

directory = os.getcwd()

def main():
    '''
    Input: Table of the simulation design after the clustering analysis
    Output: Dataframe with r_limit calculated for each combination of parameters of the given simulation design
    '''
    # Time of execution
    start = time.time()
    
    # Cycle parameter: "With" or "Without" host alternation
    CYCLE = "Without"
    # Iteration index
    index = 0
    
    # Import the simulation design from the clustering analysis 
    with open("".join([directory,"/Data/Table_params_clusters_RandomDesign_",CYCLE,"_host_alternation.csv"]),"r") as f_in, open("".join([directory,"/Data/Table_params_clusters_rlimite_RandomDesign_",CYCLE,"_host_alternation.csv"]),"w") as f_out:
        for ligne in iter(f_in.readline,""):
            vecteur_ligne = ligne.split(',')
            
            if(index==0):
                f_out.write(ligne.replace('\n',', r_limite\n'))
                index +=1
            else:
                print(index)
                tmig = float(vecteur_ligne[1])
                propR = float(vecteur_ligne[4])
                size_tot = 10000
                N0 = round((1-propR)*size_tot)
                r_init = 3
                r_step = -0.001
                r_fin = 1
                SELECT_REPRO = 0.75
                GEN = 400
                
                # Calculate the growth rate threshold 'r_limite'
                r_lim = r_limite(Cycle=CYCLE, N0=N0, KS=N0, KR=round(propR*size_tot), MIG=tmig, GEN=GEN, SELECT_REPRO=SELECT_REPRO,r_init=r_init, r_fin=r_fin, r_step=r_step)
                # Implement the output table
                f_out.write(ligne.replace('\n',', '+str(r_lim)+ '\n'))
                index+=1
    # Print the execution time
    end = time.time()
    elapsed = end - start
    print(elapsed)
    return()



def r_limite(Cycle, N0, KS, KR, MIG, GEN, SELECT_REPRO,r_init, r_fin, r_step):
    '''
    Inputs: 
        Cycle: life cycle, 'With' or 'Without' host alternation
        N0: Initial population size 
        KS: Carrying capacity of the Susceptible compartment
        KR: Carrying capacity of the Resistant compartment
        MIG: migration rate (mortality rate at each generation here because there is no virulent allele)
        GEN: Number of years to simulate (11 generations in one year)
        SELECT_REPRO: survival rate each year due to the overwintering
        r_init, r_fin, r_step: range of growth rate values to test (r_init: initial value, r_fin: last value, r_step: step between each test)
    
    Output: 
        Value of r corresponding to the growthrate threshold of evolutionary rescue 'r_limite' for the given set of parameters
    '''
    if Cycle == "With":
        for r in np.arange(r_init, r_fin, r_step) :
            # First generation: exponential growth on the alternate host
            N = r*N0

            for k in range(GEN):
                # Demographic steps during one simulated year, with host alternation
                N =  evolution_annee_avec_alt(N, KS, r, MIG, KR, SELECT_REPRO)
            
            # If the population size is close enough to 0, extinction: the growthrate threshold 'r_limite' has been reached, print 'ok' and return this growth rate
            if round(N) == 0 :
                print("ok")
                return(r)

    elif Cycle=="Without":
        for r in np.arange(r_init, r_fin, r_step) :
            # First generation: logistic growth on the susceptible host
            N = logistique_migration(N0, KS, r, MIG)

            for k in range(GEN):
                # Demographic steps during one simulated year, without host alternation
                N =  evolution_annee_sans_alt(N, KS, r, MIG, SELECT_REPRO)

            # If the population size is close enough to 0, extinction: the growthrate threshold 'r_limite' has been reached, print 'ok' and return this growth rate
            if round(N) == 0 :
                print("ok")
                return(r)
    
    # If no growth rate in the given range led to a population extinction, there is no solution (you have to define a larger range for r, with lower values to test, or increase the number of simulated generations)
    return("No solution")


def logistique_migration(N, KS, r, MIG):
    '''
    Calculate the new population size after one generation (migration + reproduction)
    
    Input:
        N: population size at the start of the generation
        KS: Carrying capacity of the Susceptible compartment
        r: population growth rate
        MIG: migration rate (mortality rate at each generation here because there is no virulent allele)
    
    Output: 
        New population size after one generation (migration + reproduction)
    '''
    # New population size after the migration
    Nmig = N-N*MIG
    # If the population size exceed the carrying capacity, remove supplement individuals
    if Nmig>KS :
        Nmig = KS
        # New population size after the reproduction (logistic growth)
    Nlog = Nmig + (r-1)*Nmig*(1-(Nmig/KS))
    return(Nlog)

def evolution_annee_avec_alt(N, KS, r, MIG, KR, SELECT_REPRO):
            '''
            Calculate the new population size after one year, with host alternation
            
            Input:
                N: population size at the start of the generation
                KS: Carrying capacity of the Susceptible compartment
                r: population growth rate
                MIG: migration rate (mortality rate at each generation here because there is no virulent allele)
                KR: Carrying capacity of the Resistant compartment
                SELECT_REPRO: mortality rate each year due to the overwintering                
            
            Output: 
                New population size after one simulated year 
            '''
            # New population size on S (everyone die on R, no virulent allele) after the annual redistribution of individuals from the alternate host
            N = N*(KS/(KS+KR))

            # New population size after successive generations of migration + logistic growth
            for i in range(9):
                N = logistique_migration(N,KS,r, MIG)

            # New population size after the annual mortality due to overwintering: only a proportion SELECT_REPRO of the population survives
            N = SELECT_REPRO*N

            # New population size after the exponential growth on the alternate host
            N = r*N

            return(N)

def evolution_annee_sans_alt(N, KS, r, MIG, SELECT_REPRO):
            '''
            Calculate the new population size after one year, without host alternation 
            
            Input:
                N: population size at the start of the generation
                KS: Carrying capacity of the Susceptible compartment
                r: population growth rate
                MIG: migration rate (mortality rate at each generation here because there is no virulent allele)
                SELECT_REPRO: mortality rate each year due to the overwintering                
            
            Output: 
                New population size after one simulated year
            '''
            # New population size after successive generations of migration + logistic growth
            for i in range(10):
                N = logistique_migration(N,KS,r, MIG)

            # New population size after the annual mortality due to overwintering: only a proportion SELECT_REPRO of the population survives
            N = SELECT_REPRO*N

            return(N)

if __name__ == "__main__":
    main()
