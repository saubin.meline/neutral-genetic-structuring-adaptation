Codes relating to the article "Neutral genetic structuring of pathogen populations during rapid adaptation".

These codes are designed to perform all statistic and clustering analyses presented in the article, and to plot all main and appendix graphics and tables related to the article.

A first description of each script is provided by alphabetical order. Finally, a general description of the project is provided with the order in which the scripts must be executed to obtain the figures and data presented in the article.

# Script Descriptions

1. `Clustering.R`
	
	Script to study the proportions of evolutionary rescue events in each cluster, based on the random simulation design.

	**Inputs:** 
	- The chosen life cycle of the pathogen (Cycle 'With' or 'Without' host alternation)
	- The chosen simulation design ('Random' or 'Regular')
	- If already calculated, the distance matrix among simulations DistanceMatrix_SimulationDesign_Cycle_host_alternation.Rdata
	- The input table TemporaryTable_RandomDesign.Rdata or TemporaryTable_RegularDesign_Cycle_host_alternation.Rdata depending on the simulation design and life cycle (from script `Convert_Dataframe_RandomDesign.R` or `Convert_Dataframe_RegularDesign.R`)

	**Outputs:**
	- The distance matrix among simulations DistanceMatrix_SimulationDesign_Cycle_host_alternation.Rdata
	- Figure of the clustering dendrogram and evolution of population genetics indices (Figure 3)
	- Tables of the cluster assigned to each simulation Table_params_clusters_SimulationDesign_Cycle_host_alternation.Rdata, Table_params_clusters_SimulationDesign_Cycle_host_alternation.csv, and Table_indices_clusters_SimulationDesign_Cycle_host_alternation.Rdata
	- Tables of the corresponding medoids for each cluster Table_params_medoids_SimulationDesign_Cycle_host_alternation.Rdata and Table_params_medoids_SimulationDesign_Cycle_host_alternation.csv

2. `Comparison_Overcoming_NullModel.R`
	
	Script to compare medoid simulations and the corresponding null model simulations. Plot graphics for supplementary figures S4, S5, S6, S7 and S8.

	**Inputs:** 
	- The chosen life cycle of the pathogen (Cycle 'With' or 'Without' host alternation)
	- Table_params_medoids_RandomDesign_Cycle_host_alternation.Rdata (from script `Clustering.R`)
	- TemporaryTable_RandomDesign.Rdata and TemporaryTableParameters_Complete_RandomDesign.Rdata (from script `Convert_Dataframe_RandomDesign.R`)
	- The input table Output_RandomDesign_Mnull_Medoid_Cycle_host_alternation.txt

	**Outputs:**
	- Supplementary figures S4, S5, S6, S7 and S8

3. `Convert_Dataframe_RandomDesign.R`
	
	For the random simulation design, script to adapt the initial dataframe output from the python demogenetic script, to a R dataframe suitable for clustering analyses.

	**Input:** 
	- The input table Output_RandomDesign.txt from the random simulation design, result from the python script [...]

	**Outputs:**
	- R Dataframes adapted to further clustering and graphical analyses TemporaryTable_RandomDesign.Rdata, TemporaryTableParameters_Complete_RandomDesign.Rdata and TemporaryTableParameters_RandomDesign.Rdata


4. `Convert_Dataframe_RegularDesign.R`
	
	For the regular simulation design, script to adapt the initial dataframe output from the python demogenetic script, to a R dataframe suitable for clustering analyses.

	**Inputs:** 
	- The chosen life cycle of the pathogen (Cycle 'With' or 'Without' host alternation)
	- The input table Output_RegularDesign_Cycle_host_alternation.txt from the regular simulation design (adapted to the chosen life cycle), result from the python script [...]

	**Outputs:**
	- R Dataframes adapted to further clustering and graphical analyses TemporaryTable_RegularDesign_Cycle_host_alternation.Rdata and TemporaryTableParameters_RegularDesign_Cycle_host_alternation.Rdata (adapted to the chosen life cycle)

5. `Demogenetic_model.exe`

	Executable file that takes as input a simulation design (input.txt), simulate the pathogen population evolution through time and give as output the temporal evolution of a set of population genetic indices on three host compartment: susceptible (S), resistant (R) and alternate host (A).

	**Inputs:** 
	Table of simulation design (named input.txt), with the following variables:
	- alternance: life cycle of the pathogenm with (True) or without (False) host alternation (Cycle)
	- tu: mutation rate of the neutral markers
	- tmig: migration rate of the pathogen between R and S host compartments (mig)
	- nbgenbi: number of generations to simulate for the burn-in period
	- nbgen: number of generations to simulate after the burn-in period
	- nblociN: number of neutral loci to simulate
	- talvir: initial proportion of virulent alleles in the pathogen population (favr)
	- nbcapchaSetR: cumulated carrying capacyty on S and R host compartments (K)
	- nbcapchaS: carrying capacity on the S compartment (KS)
	- nbcapchaR: carrying capacity on the R compartment (KR)
	- tr: pathogen population growth rate (r)
	- tmortMel: mortality rate during the annual migration (tau)

	**Outputs:**
	- Simulation results in a .txt file

6. `Distribution_Regular_Design.R`
	
	Script to analyse the distribution of simulations from the regular simulation design composed of 100 replicates of 216 fixed combinations of parameter values.

	**Inputs:** 
	- The chosen life cycle of the pathogen (Cycle 'With' or 'Without' host alternation)
	- The input table Table_params_clusters_RegularDesign_Cycle_host_alternation.Rdata from the regular simulation design (adapted to the chosen life cycle) with the assignation of each simulation to one cluster (result from script `Clustering.R`)

	**Outputs:**
	- Table of the cluster description for the clustering based on the regular simulation design (Table S2)

7. `Dominance_Analyses_and_Graphics.R`
	
	Script to perform the dominance analysis to asses the influence of the 4 input parameters (favr, m, r, propR) on the simulation clustering.

	**Inputs:** 
	- List of parameters to perform the dominance analysis on and to plot
	- The chosen life cycle of the pathogen (Cycle 'With' or 'Without' host alternation)
	- The input table Table_params_clusters_RandomDesign_Cycle_host_alternation.Rdata from the random simulation design (adapted to the chosen life cycle) with the assignation of each simulation to one cluster (result from script `Clustering.R`)

	**Outputs:**
	- Figures of the dominance analyses (Figure S2)
	- Figures of the violin plots with Kruskall-Wallis tests (Figure 4)
	- Figures of the distribution of propR and favr input parameters and their assignation to different clusters (Figure S3)

8. `Graphics_MeanTemporalDynamics.R`
	
	Script to plot graphics of the mean temporal dynamics of population genetics indices in each cluster (Figures 2 and 5).

	**Inputs:** 
	- The chosen life cycle of the pathogen (Cycle 'With' or 'Without' host alternation)
	- The chosen simulation design ('Random' to plot figures 2 and 5, 'Regular' to plot additionnal figures not shown in the article)
	- The input table Table_indices_clusters_PlanDesign_Cycle_host_alternation.Rdata (adapted to the chosen simulation design and life cycle) with the assignation of each simulation to one cluster (result from script `Clustering.R`)

	**Outputs:**
	- Figures of mean temporal dynamics of population genetics indices in each cluster presented in the article (Figures 2 and 5)
	- For other indices and results from the regular simulation design, Figures of mean temporal dynamics not presented in the article 

9. `r_limite.py`
	
	Script to calculate the growth rate threshold 'r_limite', from an input combination of parameters, under which there is evolutionary rescue, that is, under which the population goes extinct if it does not invade the resistant compartment.

	**Inputs:** 
	- The chosen life cycle of the pathogen ('With' or 'Without' host alternation)
	- The input table Tableau_params_clusters_RandomDesign_Cycle_host_alternation.Rdata from the random simulation design (adapted to the chosen life cycle) with the assignation of each simulation to one cluster (result from script `Clustering.R`)

	**Outputs:**
	- Table Table_params_clusters_rlimite_RandomDesign_Cycle_host_alternation.csv (adapted to the chosen life cycle) with the corresponding value of r_limite for each parameter combination

10. `r_limite_analysis.R`
	
	Script to study the proportions of evolutionary rescue events in each cluster, based on the random simulation design.

	**Inputs:** 
	- The chosen life cycle of the pathogen (Cycle 'With' or 'Without' host alternation)
	- The input table Table_params_clusters_rlimite_RandomDesign_Cycle_host_alternation.csv from the random simulation design (adapted to the chosen life cycle) with the calculated value of r_limite for each combination of parameters (result from script `r_limite.py`)

	**Outputs:**
	- Table of proportions of evolutionary rescue events in each cluster (Table S3)
	- p-value of the exact test of Fisher, which corresponds to the last column of Table S3 in the article

# Provided Files Description

For reproducibility of the tables and figures, we provide all original files and temporary tables (from the script described above) to quickly plot the figures and tables presented in the article, in the following Zenodo repository: 10.5281/zenodo.13132223.
Please download all files from the Zenodo repository and place them in the Data folder of this GitLab repository.

# General Code Description


Before any analysis, please download all files from the Zenodo repository 10.5281/zenodo.13132223 and place them in the Data folder of the GitLab repository.

You can run the demogenetic simulator `Demogenetic_model.exe` with your own simulation design, or with the simulation designs used in the article (see Table 1 and Table S1). Please note that as the demogenetic model is stochastic, the same simulation design will lead to different results. This step is not needed if you aim to reproduce the results of the article, as the result files we used (Output_RandomDesign.txt, Output_RegularDesign_With_host_alternation.txt, Output_RegularDesign_Without_host_alternation.txt) are already provided in the Zenodo repository. 

To obtain the results presented in the article, scripts must be run in the following order without parameter modifications. Please note that some scripts can take a long time to be executed (from several hours to several days on a computer with 32Go RAM, Intel(R) Xeon(R) W-1390P @ 3.50GHz), they are indicated as **time-consuming** in the following description. All temporary tables from the different successive steps used in the article are provided for reproducibility. For each script to run, choose as working directory your source file location.

Run the scripts responsible for table creation and transformations:

	- `Convert_Dataframe_RandomDesign.R` (**time-consuming**)
	- `Convert_Dataframe_RegularDesign.R` (**time-consuming**)

Then, perform the clustering analysis:

	- `Clustering.R` (**time-consuming**)

Then, perform analyses relative to the growth rate threshold underneath we observe evolutionary rescue, in the following order:

	- `r_limite.py` (**time-consuming**)
	- `r_limite_analysis.R

Finally, clustering and the related graphics and tables in the main  text and the appendix are obtained by running the scripts in the following order:

	- `Distribution_Regular_Design.R`
	- `Dominance_Analyses_and_Graphics.R`
	- `Graphics_MeanTemporalDynamics.R`
	- `Comparison_Overcoming_NullModel.R`

R and package versions for R scripts:

	- R version 4.3.2 (2023-10-31)
	- ggpubr_0.6.0
	- arules_1.7-7 
	- pscl_1.5.9 
	- domir_1.1.1 
	- nnet_7.3-19
	- ggplotify_0.1.2 
	- dtwclust_5.5.12 
	- dtw_1.23-1      
	- proxy_0.4-27   
	- plotly_4.10.4   
	- gridExtra_2.3   
	- ggplot2_3.5.0